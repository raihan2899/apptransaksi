package com.maybank.apptransaksi.entity;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class History {

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private Long id;
	
	private String rekeningPengirim;
	
	private String rekeningPenerima;
	
	@CreatedDate
	@Column(name="tanggal_kirim")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tanggalKirim;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRekeningPengirim() {
		return rekeningPengirim;
	}

	public void setRekeningPengirim(String rekeningPengirim) {
		this.rekeningPengirim = rekeningPengirim;
	}

	public String getRekeningPenerima() {
		return rekeningPenerima;
	}

	public void setRekeningPenerima(String rekeningPenerima) {
		this.rekeningPenerima = rekeningPenerima;
	}

	public Date getTanggalKirim() {
		return tanggalKirim;
	}

	public void setTanggalKirim(Date tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}
}
