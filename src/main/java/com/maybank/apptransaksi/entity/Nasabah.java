package com.maybank.apptransaksi.entity;

import java.sql.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table (name = "nasabah")
public class Nasabah {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@NotEmpty
	@NotBlank(message = "Nama Tidak Boleh Kosong")
	@Size(min = 3, max = 30)
	@Column(name = "nama_lengkap", nullable = false)
	private String namaLengkap;
	
	@Column(name = "tanggal_lahir")
	private Date tanggalLahir;
	
	@NotNull
	@NotEmpty(message = "Identitas Tidak Boleh Kosong")
	@NotBlank
	@Column(name = "no_identitas", nullable = false, unique = true)
	private String noIdentitas;
	
	@NotNull
	@NotEmpty(message = "Tipe Identitas Tidak Boleh Kosong")
	@NotBlank
	@Column(name = "tipe_identitas", nullable = false)
	private String tipeIdentitas;
	
	@NotNull
	@NotEmpty(message = "Email Tidak Boleh Kosong")
	@NotBlank
	@Column(nullable = false)
	private String email;
	
	@NotNull
	@NotEmpty(message = "Contact Tidak Boleh Kosong")
	@NotBlank
	@Column(name = "no_contact", nullable = false)
	private String noContact;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "nasabah")
	private List<Rekening> list_rekening;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamaLengkap() {
		return namaLengkap;
	}

	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}

	public Date getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getNoIdentitas() {
		return noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getTipeIdentitas() {
		return tipeIdentitas;
	}

	public void setTipeIdentitas(String tipeIdentitas) {
		this.tipeIdentitas = tipeIdentitas;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNoContact() {
		return noContact;
	}

	public void setNoContact(String noContact) {
		this.noContact = noContact;
	}

	public List<Rekening> getList_rekening() {
		return list_rekening;
	}

	public void setList_rekening(List<Rekening> list_rekening) {
		this.list_rekening = list_rekening;
	}
	
}
