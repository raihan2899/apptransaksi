package com.maybank.apptransaksi.entity;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table (name = "transfer")
public class Transfer {

	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER.LAZY)
	@JoinColumn(name="pengirim_id", referencedColumnName = "id")
	private Rekening rekening_pengirim;
	
	@ManyToOne(fetch=FetchType.EAGER.LAZY)
	@JoinColumn(name="penerima_id", referencedColumnName = "id")
	private Rekening rekening_penerima;
	
	@CreatedDate
	@Column(name="tanggal_kirim")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date tanggalKirim;

	@NotNull(message = "Amount Tidak Boleh Kosong")
	private Double amount;
	
	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	private Double fee;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Rekening getRekening_pengirim() {
		return rekening_pengirim;
	}

	public void setRekening_pengirim(Rekening rekening_pengirim) {
		this.rekening_pengirim = rekening_pengirim;
	}

	public Rekening getRekening_penerima() {
		return rekening_penerima;
	}

	public void setRekening_penerima(Rekening rekening_penerima) {
		this.rekening_penerima = rekening_penerima;
	}

	public Date getTanggalKirim() {
		return tanggalKirim;
	}

	public void setTanggalKirim(Date tanggalKirim) {
		this.tanggalKirim = tanggalKirim;
	}

	public Double getFee() {
		return fee;
	}

	public void setFee(Double fee) {
		this.fee = fee;
	}
	
	
	
}
