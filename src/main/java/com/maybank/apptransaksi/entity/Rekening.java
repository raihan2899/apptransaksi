package com.maybank.apptransaksi.entity;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Entity
@Table (name = "rekening")
public class Rekening {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotEmpty(message = "No Rekening Tidak Boleh Kosong")
	@NotBlank
	@NotNull
	@Column(name = "no_rekening", nullable = false, unique = true)
	private String noRekening;
	
	private Double saldo = 1000000.0;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY)
	@JoinColumn(name = "provider_id", referencedColumnName = "id")
	private Provider provider;
	
	@ManyToOne(fetch = FetchType.EAGER.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "nasabah_id", referencedColumnName = "id")
	private Nasabah nasabah;

	@OneToMany(mappedBy = "rekening_pengirim")
	private List<Transfer> list_rekening_pengirim;
	
	@OneToMany(mappedBy = "rekening_penerima")
	private List<Transfer> list_rekening_penerima;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public Nasabah getNasabah() {
		return nasabah;
	}

	public void setNasabah(Nasabah nasabah) {
		this.nasabah = nasabah;
	}
	
	
}
