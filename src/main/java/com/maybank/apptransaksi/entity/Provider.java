package com.maybank.apptransaksi.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@Table (name = "provider")
public class Provider {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	@NotEmpty
	@NotBlank
	@Size(min = 2, max = 20)
	@Column(nullable = false, unique = true)
	private String name;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "provider")
	private List<Rekening> list_rekening;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Rekening> getList_rekening() {
		return list_rekening;
	}

	public void setList_rekening(List<Rekening> list_rekening) {
		this.list_rekening = list_rekening;
	}
	
	
	
}
