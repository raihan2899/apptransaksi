package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.entity.Transfer;


public interface TransferService {
	
	public Page<Transfer> getPaginateSearch(int pageNo, int pageSize, String field, String keyword);
	public Page<Transfer> getAllPaginate(int pageNo, int pageSize, String field);
	public List<Transfer> getAll();
	public void save(Transfer transfer);
	public Optional<Transfer> getTransferById(Long id);
	public void delete(Long id);
	public void getById(Long id);

}
