 package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.apptransaksi.entity.Provider;

public interface ProviderService {

	public List<Provider> getAll();
	public Page<Provider> getAllPaginate (int pageNo, int pageSize, String field);
	public void save(Provider provider);
	public void delete(Long id);
	public Optional<Provider> getProviderById(Long id);
    public Page<Provider> searchProvider(int pageNo, int pageSize, String sortField, String keyword);
}
