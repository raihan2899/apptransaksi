package com.maybank.apptransaksi.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.entity.Transfer;
import com.maybank.apptransaksi.repository.RekeningRepo;
import com.maybank.apptransaksi.repository.TransferRepo;

@Service
@Transactional
public class TransferServiceImpl implements TransferService{

	@Autowired
	private TransferRepo transferRepo;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Override
	public void save(Transfer transfer) {
		// TODO Auto-generated method stub
		Rekening rekPengirim = rekeningService.findByNoRekening(transfer.getRekening_pengirim().getNoRekening());
		Rekening rekPenerima = rekeningService.findByNoRekening(transfer.getRekening_penerima().getNoRekening());
		
		Long idSender =rekPengirim.getId();
		Long idReceiver = rekPenerima.getId();
		
		Optional<Rekening> sender = this.rekeningService.getRekeningById(idSender);
		Optional<Rekening> receiver = this.rekeningService.getRekeningById(idReceiver);
		
		Rekening rekSender = sender.get();
		Rekening rekReceive = receiver.get();
		
		String bankSender = rekSender.getProvider().getName();
		String bankReceiver = rekReceive.getProvider().getName();
		
		Transfer transfer2 = new Transfer();
		transfer2.setAmount(transfer.getAmount());
		transfer2.setTanggalKirim(Date.valueOf(LocalDate.now()));
		
		
		
		if(bankSender.equals(bankReceiver)) {
			transfer2.setFee(0.0);
			rekSender.setSaldo(rekSender.getSaldo()-(transfer2.getAmount()));
			transfer2.setRekening_pengirim(rekSender);
			transfer2.setRekening_penerima(rekReceive);
			rekReceive.setSaldo(rekReceive.getSaldo()+(transfer2.getAmount()));
			
			this.rekeningService.save(rekSender);
			this.rekeningService.save(rekReceive);
			this.transferRepo.save(transfer2);
		}else {
			transfer2.setFee(6500.0);
			rekSender.setSaldo(rekSender.getSaldo()-(transfer2.getAmount()+6500));
			transfer2.setRekening_pengirim(rekSender);
			transfer2.setRekening_penerima(rekReceive);
			rekReceive.setSaldo(rekReceive.getSaldo()+(transfer2.getAmount()));
			
			this.rekeningService.save(rekSender);
			this.rekeningService.save(rekReceive);
			this.transferRepo.save(transfer2);
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.transferRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.transferRepo.findById(id);
	}

	@Override
	public List<Transfer> getAll() {
		// TODO Auto-generated method stub
		return this.transferRepo.findAll();
	}

	@Override
	public Optional<Transfer> getTransferById(Long id) {
		// TODO Auto-generated method stub
		return this.transferRepo.findById(id);
	}
	
	@Override
	public Page<Transfer> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Transfer> getPaginateSearch(int pageNo, int pageSize, String field, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.transferRepo.search(keyword, paging);
	}

}
