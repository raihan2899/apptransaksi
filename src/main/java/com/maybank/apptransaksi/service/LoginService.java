package com.maybank.apptransaksi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.apptransaksi.entity.User;
import com.maybank.apptransaksi.repository.RoleRepo;
import com.maybank.apptransaksi.repository.UserRepo;

@Service
public class LoginService {

	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private RoleRepo roleRepo;
	
	public User findUserByUsername(String username) {
		return this.userRepo.findByUsername(username);
	}

	public void addUser(User user) {
		// TODO Auto-generated method stub
		this.userRepo.save(user);
	}

	public List<User> findAll() {
		// TODO Auto-generated method stub
		return this.userRepo.findAll();
	}
}