package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import jakarta.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.repository.ProviderRepo;

@Service
@Transactional
public class ProviderServiceImpl implements ProviderService{

	@Autowired
	private ProviderRepo providerRepo;
	
	@Override
	public List<Provider> getAll() {
		// TODO Auto-generated method stub
		return this.providerRepo.findAll();
	}

	@Override
	public Page<Provider> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.providerRepo.findAll(paging);
	}

	@Override
	public void save(Provider provider) {
		// TODO Auto-generated method stub
		this.providerRepo.save(provider);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.providerRepo.deleteById(id);
	}

	@Override
	public Optional<Provider> getProviderById(Long id) {
		// TODO Auto-generated method stub
		return this.providerRepo.findById(id);
	}

	@Override
	public Page<Provider> searchProvider(int pageNo, int pageSize, String sortField, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
        return this.providerRepo.findByKeyword(paging, keyword);
	}


	
}
