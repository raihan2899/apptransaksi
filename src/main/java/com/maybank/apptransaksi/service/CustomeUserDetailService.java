package com.maybank.apptransaksi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.apptransaksi.entity.Role;

@Service
public class CustomeUserDetailService implements UserDetailsService{
	
	@Autowired
	private LoginService loginService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
		
		com.maybank.apptransaksi.entity.User userApp = this.loginService.findUserByUsername(username);
		List<GrantedAuthority> auths = new ArrayList<>();
		
		if(userApp != null) {
			List<Role> roles = userApp.getRoles();
			if(roles.size()>0) {
				for(Role role:roles) {
					auths.add(new SimpleGrantedAuthority(role.getRole()));
					auths.add(new SimpleGrantedAuthority(role.getRole()));
				}
			}
			UserDetails user = User.withUsername(userApp.getUsername())
					.password(userApp.getPassword())
					.authorities(auths)
					.build();
			
			return user;
		}else {
			throw new UsernameNotFoundException("User not Found!");
		}
	}
	
}
