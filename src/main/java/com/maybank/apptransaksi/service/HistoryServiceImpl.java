package com.maybank.apptransaksi.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.maybank.apptransaksi.entity.History;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.entity.Transfer;
import com.maybank.apptransaksi.repository.HistoryRepo;

import org.springframework.data.domain.Sort;

@Service
public class HistoryServiceImpl implements HistoryService{

	@Autowired
	private HistoryRepo historyRepo;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Override
	public Page<History> getPaginateSearch(int pageNo, int pageSize, String field, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.historyRepo.search(keyword, paging);
	}

	@Override
	public Page<History> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.historyRepo.findAll(paging);
	}

	@Override
	public List<History> getAll() {
		// TODO Auto-generated method stub
		return this.historyRepo.findAll();
	}

	@Override
	public void save(History history, Transfer transfer) {
		// TODO Auto-generated method stub
		Rekening rekeningPengirim = this.rekeningService.findByNoRekening(transfer.getRekening_pengirim().getNoRekening());
		Rekening rekeningPenerima = this.rekeningService.findByNoRekening(transfer.getRekening_penerima().getNoRekening());
		System.out.println(rekeningPengirim);
		System.out.println(rekeningPenerima);
		history.setRekeningPenerima(rekeningPenerima.getNoRekening());
		history.setRekeningPengirim(rekeningPengirim.getNoRekening());
		history.setTanggalKirim(Date.valueOf(LocalDate.now()));
		this.historyRepo.save(history);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.historyRepo.deleteById(id);
	}

	@Override
	public void getById(Long id) {
		// TODO Auto-generated method stub
		this.historyRepo.findById(id);
	}

	@Override
	public Optional<History> getRekeningById(Long id) {
		// TODO Auto-generated method stub
		return this.historyRepo.findById(id);
	}


}
