package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.apptransaksi.entity.Rekening;

public interface RekeningService{
	
	public List<Rekening> getAll();
	public Page<Rekening> getAllPaginate (int pageNo, int pageSize, String field);
	public void save(Rekening rekening);
	public void delete(Long id);
	public Rekening findByNoRekening(String noRekening);
	public Optional<Rekening> getRekeningById(Long id);
//    public Page<Rekening> searchRekening(int pageNo, int pageSize, String sortField, String keyword);
    

}
