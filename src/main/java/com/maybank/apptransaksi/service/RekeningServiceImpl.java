package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.repository.RekeningRepo;

@Service
@Transactional
public class RekeningServiceImpl implements RekeningService{

	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Override
	public List<Rekening> getAll() {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findAll();
	}

	@Override
	public Page<Rekening> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.rekeningRepo.findAll(paging);
	}

	@Override
	public void save(Rekening rekening) {
		// TODO Auto-generated method stub
		this.rekeningRepo.save(rekening);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.rekeningRepo.deleteById(id);
	}

	@Override
	public Optional<Rekening> getRekeningById(Long id) {
		// TODO Auto-generated method stub
		return this.rekeningRepo.findById(id);
	}

	@Override
	public Rekening findByNoRekening(String noRekening) {
        // TODO Auto-generated method stub
        return this.rekeningRepo.findByNoRekening(noRekening);
    }

}
