package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maybank.apptransaksi.entity.Nasabah;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.repository.NasabahRepo;
import com.maybank.apptransaksi.repository.RekeningRepo;

@Service
@Transactional
public class NasabahServiceImpl implements NasabahService{

	@Autowired
	private NasabahRepo nasabahRepo;
	
	@Autowired
	private RekeningRepo rekeningRepo;
	
	@Override
	public List<Nasabah> getAll() {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findAll();
	}

	@Override
	public Page<Nasabah> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.nasabahRepo.findAll(paging);
	}

	@Override
	public void save(Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.nasabahRepo.save(nasabah);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		this.nasabahRepo.deleteById(id);
	}

	@Override
	public Optional<Nasabah> getNasabahById(Long id) {
		// TODO Auto-generated method stub
		return this.nasabahRepo.findById(id);
	}

	@Override
	public Page<Nasabah> searchNasabah(int pageNo, int pageSize, String sortField, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(sortField).ascending());
        return this.nasabahRepo.findByKeyword(paging, keyword);
	}

	@Override
	public void addNasabah(Provider provider, Rekening rekening, Nasabah nasabah) {
		// TODO Auto-generated method stub
		this.nasabahRepo.save(nasabah);
		rekening.setNasabah(nasabah);
		rekening.setProvider(provider);
		this.rekeningRepo.save(rekening);
	}


}
