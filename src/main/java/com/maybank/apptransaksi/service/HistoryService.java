package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.apptransaksi.entity.History;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.entity.Transfer;

public interface HistoryService {
	public Page<History> getPaginateSearch(int pageNo, int pageSize, String field, String keyword);
	public Page<History> getAllPaginate(int pageNo, int pageSize, String field);
	public List<History> getAll();
	public void save(History history, Transfer transfer);
	public void delete(Long id);
	public void getById(Long id);
	public Optional<History> getRekeningById(Long id);
}
