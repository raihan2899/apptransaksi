package com.maybank.apptransaksi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.apptransaksi.entity.Nasabah;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.entity.Transfer;

public interface NasabahService {

	public List<Nasabah> getAll();
	public Page<Nasabah> getAllPaginate (int pageNo, int pageSize, String field);
	public void save(Nasabah nasabah);
	public void delete(Long id);
	public Optional<Nasabah> getNasabahById(Long id);
    public Page<Nasabah> searchNasabah(int pageNo, int pageSize, String sortField, String keyword);
    public void addNasabah(Provider provider, Rekening rekening, Nasabah nasabah);
}
