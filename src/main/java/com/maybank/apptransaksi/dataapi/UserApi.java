package com.maybank.apptransaksi.dataapi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.maybank.apptransaksi.entity.User;
import com.maybank.apptransaksi.service.LoginService;

@RestController
@RequestMapping("/api")
public class UserApi {

	@Autowired
	private LoginService loginService;
	
	@GetMapping("/user")
	public List<User> index() {
		return this.loginService.findAll();
	}
	
//	saving data
	@PostMapping("/user/save")
	@ResponseStatus(HttpStatus.CREATED)
	public void saveUser(@RequestBody User user) {
		this.loginService.addUser(user);
//		return user;
	}
}
