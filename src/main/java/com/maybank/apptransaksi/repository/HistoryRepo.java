package com.maybank.apptransaksi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maybank.apptransaksi.entity.History;

public interface HistoryRepo extends JpaRepository<History, Long>{
	@Query
	(value = "SELECT * FROM history s where s.tanggal_kirim iLIKE %:keyword%", nativeQuery = true)
    Page<History>search(@Param("keyword") String keyword, Pageable pageable);
}
