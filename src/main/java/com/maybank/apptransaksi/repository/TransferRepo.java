package com.maybank.apptransaksi.repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.entity.Transfer;

public interface TransferRepo extends JpaRepository<Transfer, Long>{
	@Query
	(value = "SELECT * FROM transfer t where t.amount iLIKE %:keyword%" + 
			 "SELECT * FROM transfer t WHERE t.fee iLIKE %:keyword%" + 
			 "SELECT * FROM transfer t WHERE t.tanggal_kirim iLIKE %:keyword%", nativeQuery = true)
    Page<Transfer>search(@Param("keyword") String keyword, Pageable pageable);
}