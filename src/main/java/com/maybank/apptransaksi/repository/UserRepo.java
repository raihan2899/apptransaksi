package com.maybank.apptransaksi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.apptransaksi.entity.User;

public interface UserRepo extends JpaRepository<User, Long>{

	User findByUsername(String username);
	
}
