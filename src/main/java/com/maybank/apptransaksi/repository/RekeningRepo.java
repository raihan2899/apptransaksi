package com.maybank.apptransaksi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maybank.apptransaksi.entity.History;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.entity.Rekening;

public interface RekeningRepo extends JpaRepository<Rekening, Long>{
	@Query
	(value = "SELECT * FROM rekening s where s.no_rekening iLIKE %:keyword%", nativeQuery = true)
    Page<Rekening>search(@Param("keyword") String keyword, Pageable pageable);
	Rekening findByNoRekening(String noRekening);
}
