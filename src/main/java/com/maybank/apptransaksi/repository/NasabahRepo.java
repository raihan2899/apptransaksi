package com.maybank.apptransaksi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maybank.apptransaksi.entity.Nasabah;
import com.maybank.apptransaksi.entity.Provider;

public interface NasabahRepo extends JpaRepository<Nasabah, Long>{

	@Query
	(value = "SELECT * FROM nasabah s where s.nama_lengkap iLIKE %:keyword%", nativeQuery = true)
    Page<Nasabah>findByKeyword(Pageable pageable, 
    
    @Param("keyword") 
    String keyword);
}
