package com.maybank.apptransaksi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.apptransaksi.entity.Role;


public interface RoleRepo extends JpaRepository<Role, Long>{
	//List<Role> findByUsers(User user);
}
