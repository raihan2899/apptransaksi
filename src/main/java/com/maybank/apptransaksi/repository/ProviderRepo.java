package com.maybank.apptransaksi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maybank.apptransaksi.entity.Provider;

public interface ProviderRepo extends JpaRepository<Provider, Long>{

	@Query
	(value = "SELECT * FROM provider s where s.name iLIKE %:keyword%", nativeQuery = true)
    Page<Provider>findByKeyword(Pageable pageable, 
    
    @Param("keyword") 
    String keyword);
}
