package com.maybank.apptransaksi.helper;
import java.io.IOException;
import java.util.Collection;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;


@Component
public class UserAuthenticationSuccessHandler implements AuthenticationSuccessHandler{
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		// TODO Auto-generated method stub
		boolean hasCsRole = false;
		boolean hasAdminRole = false;
		boolean hasOperatorRole = false;
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		for (GrantedAuthority grantedAuthority : authorities) {
			if (grantedAuthority.getAuthority().equals("cservice")) {
				hasCsRole = true;
				break;
			} else if (grantedAuthority.getAuthority().equals("admin")) {
				hasAdminRole = true;
				break;
			} else if (grantedAuthority.getAuthority().equals("operator")) {
				hasOperatorRole = true;
				break;
			}
		}

		if (hasCsRole) {
			redirectStrategy.sendRedirect(request, response, "/rekening");
		} else if (hasAdminRole) {
			redirectStrategy.sendRedirect(request, response, "/provider");
		}
		else if(hasOperatorRole) {
			redirectStrategy.sendRedirect(request, response, "/transfer");
		}
		else {
			throw new IllegalStateException();
		}
	}
	

}

