package com.maybank.apptransaksi.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.data.domain.Page;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.apptransaksi.entity.History;
import com.maybank.apptransaksi.entity.Nasabah;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.entity.Transfer;
import com.maybank.apptransaksi.service.HistoryService;
import com.maybank.apptransaksi.service.NasabahService;
import com.maybank.apptransaksi.service.ProviderService;
import com.maybank.apptransaksi.service.RekeningService;
import com.maybank.apptransaksi.service.TransferService;

@Controller
@RequestMapping("/transfer")
public class TransferController {
	
	@Autowired
	private TransferService transferService;

	@Autowired
	private NasabahService nasabahService;
	
	@Autowired
	private RekeningService rekeningService;
	

	@Autowired
	private HistoryService historyService;
	
	@GetMapping
	public String index(@RequestParam(required = false) String keyword,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,Model model) {
		Page<History> listHistory;
		Page<Transfer> listTransfer;
		List<Transfer> listTransfers;
		List<Rekening> rekenings = this.rekeningService.getAll();
		
		if(keyword == null) {
			listTransfer = this.transferService.getAllPaginate(pageNo, pageSize, sortField);
		}else {
			listTransfer = this.transferService.getPaginateSearch(pageNo, pageSize, sortField, keyword);
		}
		listTransfers = this.transferService.getAll();
		model.addAttribute("transferForm", new Transfer());
		model.addAttribute("historyForm", new History());
		model.addAttribute("keyword", keyword);
		model.addAttribute("page", listTransfers);
		model.addAttribute("rekeningList", rekenings);
		return "transfer";
		
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("transferForm") Transfer transfer,BindingResult result,
			@Valid @ModelAttribute("historyForm") History history,BindingResult result1, RedirectAttributes redirectAttributes,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			Model model) {
		Transfer tran = transfer;
		Page<Transfer> listTransfer = this.transferService.getAllPaginate(pageNo, pageSize, sortField);
		List<Rekening> listRek = this.rekeningService.getAll();

		if(result.hasErrors() || result1.hasErrors()) {
			List<Rekening> rekeningss = this.rekeningService.getAll();

			model.addAttribute("transferForm", transfer);
			model.addAttribute("rekeningList", rekeningss);
			redirectAttributes.addFlashAttribute("warning", "No Rekening Tidak Ada!");
			return "transfer";
		}
		
			this.transferService.save(tran);
			this.historyService.save(history, tran);
			redirectAttributes.addFlashAttribute("success", "Transfer Berhasil!");
			return "redirect:/transfer";

	}

	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<Transfer> transfer = this.transferService.getTransferById(id);
		if(transfer.isPresent()){
			this.transferService.delete(id);
		}
		return "redirect:/transfer";
	}
}
