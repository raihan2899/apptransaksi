package com.maybank.apptransaksi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.apptransaksi.entity.History;
import com.maybank.apptransaksi.service.HistoryService;


@Controller
@RequestMapping("/history")
public class HistoryController {
	
	@Autowired
	private HistoryService historyService;
	
	@GetMapping
	public String index(Model model) { 
		List<History> listHistory = this.historyService.getAll();
        model.addAttribute("page", listHistory);
		return "history";
	}
	
	@GetMapping("/delete")
	public String delete(History history, RedirectAttributes redirectAttributes) {
		this.historyService.delete(history.getId());
		return "redirect:/history";
	}
}
