package com.maybank.apptransaksi.controller;

import java.util.List;
import java.util.Optional;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.apptransaksi.entity.Nasabah;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.entity.Rekening;
import com.maybank.apptransaksi.repository.ProviderRepo;
import com.maybank.apptransaksi.service.NasabahService;
import com.maybank.apptransaksi.service.ProviderService;
import com.maybank.apptransaksi.service.RekeningService;

@Controller
@RequestMapping("/rekening")
public class RekeningController {

	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private RekeningService rekeningService;
	
	@Autowired
	private NasabahService nasabahService;
	

	@GetMapping
	public String index(
			@RequestParam(value="pageNo", defaultValue="0")int pageNo,
            @RequestParam(value="pageSize", defaultValue="2")int pageSize,
            @RequestParam(value="sortField", defaultValue="id")String sortField,
            @RequestParam(value="keyword", defaultValue = "") String keyword,
            Model model) {
		
		Page<Nasabah> listnasabah;
		List<Provider> providers = this.providerService.getAll();
		List<Rekening> rekenings = this.rekeningService.getAll();
		
		if(keyword=="") {
			listnasabah = this.nasabahService.getAllPaginate(pageNo, pageSize, sortField);
        }
        else {
        	listnasabah = this.nasabahService.searchNasabah(pageNo, pageSize, sortField, keyword);
        }
		
		model.addAttribute("providers", providers);
		model.addAttribute("nasabah", new Nasabah());
		model.addAttribute("rekening", new Rekening());
		model.addAttribute("rekeningList", rekenings);
        model.addAttribute("page", listnasabah);
		
		return "rekening";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("nasabah") Nasabah nasabah,
			BindingResult result,
			@Valid @ModelAttribute("rekening") Rekening rekening,
			BindingResult result2,
			@Valid @ModelAttribute("providers") Provider provider,
			BindingResult result3, 
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "2") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			Model model) {
		
		if(result.hasErrors() || result2.hasErrors() || result3.hasErrors()) {
			Page<Nasabah> nasabahs = this.nasabahService.getAllPaginate(pageNo, pageSize, sortField);
			List<Provider> providers = this.providerService.getAll();
			
			model.addAttribute("page", nasabahs);
			model.addAttribute("nasabah", nasabah);
			model.addAttribute("rekening", rekening);
			model.addAttribute("providers", providers);

			return "rekening";
		}
		
		System.out.println("nasabah :"+ nasabah.getNamaLengkap());
		
		this.nasabahService.addNasabah(provider, rekening, nasabah);
		redirectAttributes.addFlashAttribute("success", "Data Inserted!");
		return "redirect:/rekening";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		this.rekeningService.delete(id);
		return "redirect:/rekening";
	}
	
	@GetMapping("/edit")
	public String edit(@RequestParam("id") Long id, Model model) {
		Optional<Nasabah> nasabah = this.nasabahService.getNasabahById(id);
		model.addAttribute("rekeningForm", nasabah);
		return "edit-rekening";
		
	}
	
}
