package com.maybank.apptransaksi.controller;

import java.util.Optional;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.service.ProviderService;

@Controller
@RequestMapping("/provider")
public class ProviderController {

	@Autowired
	private ProviderService providerService;
	
	@GetMapping
	public String index(@RequestParam(value="pageNo", defaultValue="0")int pageNo,
            @RequestParam(value="pageSize", defaultValue="2")int pageSize,
            @RequestParam(value="sortField", defaultValue="id")String sortField,
            @RequestParam(value="keyword", defaultValue = "") String keyword,
            Model model) {
		
		Page<Provider> listproviders;
		
		if(keyword=="") {
			listproviders = this.providerService.getAllPaginate(pageNo, pageSize, sortField);
        }
        else {
        	listproviders = this.providerService.searchProvider(pageNo, pageSize, sortField, keyword);
            System.out.println(listproviders.toString());
        }
		
		String springMessage = "Bank Provider";
		model.addAttribute("springMessage", springMessage);
		model.addAttribute("providerForm", new Provider());
        model.addAttribute("page", listproviders);
		
		return "provider";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("providerForm") Provider provider, 
			BindingResult result, 
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "2") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			Model model) {
		
		if(result.hasErrors()) {
			
			Page<Provider> providers = this.providerService.getAllPaginate(pageNo, pageSize, sortField);
			model.addAttribute("page", providers);
			
			return "provider";
		}
		
		System.out.println("provider :"+ provider.getName());
		
		this.providerService.save(provider);
		redirectAttributes.addFlashAttribute("success", "Data Inserted!");
		return "redirect:/provider";
	}
	
	@GetMapping("/delete")
	public String delete(Provider provider, RedirectAttributes redirectAttributes) {
		this.providerService.delete(provider.getId());
		return "redirect:/provider";
	}
	
	@GetMapping("/edit")
	public String edit(@RequestParam("id") Long id, Model model) {
		Optional<Provider> provider = this.providerService.getProviderById(id);
		model.addAttribute("providerForm", provider);
		return "edit-provider";
		
	}
}