package com.maybank.apptransaksi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import com.maybank.apptransaksi.helper.UserAuthenticationSuccessHandler;
import com.maybank.apptransaksi.service.CustomeUserDetailService;

@Configuration
@EnableWebSecurity
public class MyAuth {
	
	@Autowired
	private CustomeUserDetailService customeUserDetailService;
	
	@Autowired
	private UserAuthenticationSuccessHandler successHandler;
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(customeUserDetailService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder());
		return authProvider;
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();

	}
	
	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception{
		
		httpSecurity.authorizeHttpRequests().requestMatchers("/rekening")
			.hasAnyAuthority("cservice");
		httpSecurity.authorizeHttpRequests().requestMatchers("/provider")
			.hasAnyAuthority("admin");
		httpSecurity.authorizeHttpRequests().requestMatchers("/transfer")
			.hasAnyAuthority("operator");
		httpSecurity.authorizeHttpRequests().requestMatchers("/history")
		.hasAnyAuthority("operator");
		httpSecurity.csrf().disable();
		httpSecurity.authorizeHttpRequests().anyRequest().authenticated();
		httpSecurity.authorizeHttpRequests().and().formLogin((form) -> form
				.loginPage("/login").permitAll()).logout((logout) -> logout.permitAll());
		httpSecurity.authorizeHttpRequests().and().formLogin().successHandler(successHandler);
		return httpSecurity.build();
	}
}