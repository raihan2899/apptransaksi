package com.maybank.apptransaksi;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maybank.apptransaksi.entity.Nasabah;
import com.maybank.apptransaksi.entity.Provider;
import com.maybank.apptransaksi.repository.NasabahRepo;
import com.maybank.apptransaksi.repository.ProviderRepo;

@SpringBootApplication
public class TfappsApplication implements ApplicationRunner{

	public static void main(String[] args) {
		SpringApplication.run(TfappsApplication.class, args);
	}
	
	@Autowired
	private NasabahRepo nasabahRepo;
	
	@Autowired
	private ProviderRepo providerRepo;

	@Override
	public void run(ApplicationArguments args) throws Exception{
		
//		Provider provider = new Provider();
//		provider.setName("Maybank");
//		this.providerRepo.save(provider);
		
//		Nasabah nasabah = new Nasabah();
//		String tglLahir = "12/12/2000";
//		Date date = Date.valueOf(tglLahir);		
//		nasabah.setNamaLengkap("Elisamril Sinaga");
//		nasabah.setNoIdentitas("10000002");
//		nasabah.setTanggalLahir(null);
//		nasabah.setTipeIdentitas("KTP");
//		nasabah.setNoContact("082313213");
//		nasabah.setEmail("samril@mail.com");
//		this.nasabahRepo.save(nasabah);
	}
}
